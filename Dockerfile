FROM bitnami/spark:3.5.0

WORKDIR /app/

RUN pip install --no-cache-dir delta-spark==3.1.0

COPY ./spark .


