import pyspark.sql.functions as F
import pyspark.sql.types as T
from delta import DeltaTable
from pyspark.sql import SparkSession

if __name__ == "__main__":
    uv_table_location = "s3a://data-lakehouse/delta_tables/uv_with_generated_column"

    spark: SparkSession = SparkSession.builder.master("local[*]").getOrCreate()
    df = (
        spark.read.option("header", True)
        .csv("/test_data/uv.csv")
        .withColumns(
            {
                "time": F.col("time").cast(T.TimestampType()),
                "uv": F.col("uv").cast(T.DoubleType()),
            }
        )
    )
    print(df.schema)
    print(df.show())

    (
        DeltaTable.createIfNotExists(spark)
        .addColumn("time", "TIMESTAMP")
        .addColumn("uv", "DOUBLE")
        .addColumn("date", "DATE", generatedAlwaysAs="CAST(time AS DATE)")
        .location(uv_table_location)
        .partitionedBy("date")
        .execute()
    )

    df.write.format("delta").mode("overwrite").save(uv_table_location)
