import pyspark.sql.functions as F
import pyspark.sql.types as T
from delta import DeltaTable
from pyspark.sql import SparkSession

if __name__ == "__main__":
    uv_table_location = "s3a://data-lakehouse/delta_tables/uv_with_generated_column"

    spark: SparkSession = SparkSession.builder.master("local[*]").getOrCreate()

    spark.read.format("delta").load(uv_table_location).show()

    print("read_table ends")
