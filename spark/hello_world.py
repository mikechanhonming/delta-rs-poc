from pyspark.sql import SparkSession

if __name__ == "__main__":
    spark: SparkSession = SparkSession.builder.master("local[*]").getOrCreate()
    df = spark.range(10)
    print(df.schema)
    print(df.show())
